import React, { Component } from 'react';
import axios from 'axios'

import './App.css';

class App extends Component {
	
	state = {
	    apiUrl: 'http://webserver:8080',
		userEmail : '',
		rooms : [] ,
		nbRooms : 20,
		nbTimeslots : 24,
		isLogged: false
	}

	getBookings(roomId){
	    return axios.post(this.state.apiUrl + '/getBookings', {
            userEmail: this.state.isLogged ? this.state.userEmail : "",
            roomId: roomId
        })
	}

	updateBooking(){
	    console.log(this.state.rooms);
	    var promises = [];
	    this.state.rooms.map((roomId,i) => {
            promises.push(new Promise( resolve => this.getBookings(roomId).then(response => {
                response.data.map((timeslot, j) => {
                    var bookingId = roomId+"-"+timeslot;
                    var className = this.state.isLogged ? "owned" : "booked";
                    document.getElementById(bookingId).setAttribute("class", className);
                })
            })))
        });
	}

	manageBooking(bookingId){
	    if(this.state.userEmail === ""){
	        alert("You must be logged in to manage a booking");
	        return;
	    }

	    var element = document.getElementById(bookingId);
        var currentState = element.getAttribute("class");
        var splitResult = bookingId.split('-');

        switch(currentState){
            case "free":
                this.addBooking(splitResult[0], splitResult[1], element);
                break;
            case "owned":
                this.cancelBooking(splitResult[0], splitResult[1], element);
                break;
            case "booked":
                alert("You are not the owner of this booking");
                break;
            default:
                break;
        }
	}

	addBooking(roomId, timeSlot, element){
        element.setAttribute("class", "pending");
        axios.post(this.state.apiUrl + '/add', {
            userEmail: this.state.userEmail,
            roomId: roomId,
            timeSlot: timeSlot
        }).then(response => {
            console.log(response);
            element.setAttribute("class", "owned");
        }).catch(error => {
            element.setAttribute("class", "free");
            alert(error.response.data.message);
        });
	}

	cancelBooking(roomId, timeSlot, element){
        element.setAttribute("class", "pending");
        axios.post(this.state.apiUrl + '/cancel', {
            userEmail: this.state.userEmail,
            roomId: roomId,
            timeSlot: timeSlot
        }).then(response => {
            console.log(response);
            element.setAttribute("class", "free");
        }).catch(error => {
            element.setAttribute("class", "owned");
            alert(error.response.data.message);
        });
    }

	async componentDidMount() {
        axios.get(this.state.apiUrl + '/getRooms').then(response => {
            this.setState({rooms: response.data},() => this.updateBooking());
        });
	}

	onLogin = async (event) => {
		event.preventDefault();
		var email = document.getElementById("userEmail").value;
        this.setState({ userEmail: email , isLogged: true},() => this.updateBooking());
	}
	
	onLogout = async (event) => {
		event.preventDefault();
        document.getElementById("userEmail").value = "";
		this.setState({ userEmail: "" , isLogged: false},() => this.updateBooking());
	}
	
	render() {
	    let rows = []
        for (var i = 0; i < this.state.nbTimeslots; i++){
            let cell = []
            for (var idx = 0; idx < this.state.nbRooms + 1; idx++){
                let id = this.state.rooms[idx-1] +"-"+ i;
                if(idx === 0){
                    cell.push(<td>{i}</td>)
                }else{
                    cell.push(<td id={id} onClick={() => this.manageBooking(id)} className="free"></td>)
                }
            }
            rows.push(<tr className="room" id={this.state.rooms[idx-1]} key={i}>{cell}</tr>)
        }

        let header = [];
        for (var k = 0; k < this.state.nbRooms + 1; k++){
            if(k === 0){
                header.push(<th></th>)
            }else{
                header.push(<th>{this.state.rooms[k-1]}</th>)
            }
        }

		return (
			<div className="App">
				<header className="App-header">
				    <div className="LabelSection">
                        <p>Room Booking</p>
                        <p>COKE</p>
				    </div>
				    <div className="LoginSection">
				        <div id="login" style={{visibility: this.state.isLogged ? 'hidden' : 'visible' }}>
				            <input type="email" id="userEmail" placeholder="email"/>
				            <button onClick={this.onLogin}>Login</button>
				        </div>
				        <div id="logout" style={{visibility: this.state.isLogged ? 'visible' : 'hidden' }} >
				            <p>Connected as {this.state.userEmail}</p>
				            <button onClick={this.onLogout}>Logout</button>
				        </div>
				    </div>
				</header>

				<div id="body" className="App-body">
                    <table id="rooms">
                        <thead>
                            <tr>
                                {header}
                            </tr>
                        </thead>
                        <tbody>
                            {rows}
                        </tbody>
                    </table>
				</div>
			</div>
		);
	}
}

export default App;
