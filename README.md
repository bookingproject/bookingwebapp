# Booking WebApp

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This is the user interface to interact with the Booking Server using the Axios HTTP client.

This Web application permits you to :
* Log in using an email address 
* Book a room
* Get the state of each room at each timeslot
* Get the rooms/timeslots you (your email address) have already booked
* Cancel a booking

## How to use it

### Prerequisites

* Node JS installed
* A running Booking Server and its address

### Without Docker

Download dependencies

```
npm install
```

Run the application

```
npm start
```

Then access the default address of the web application

```
http://localhost:3000/
```

### With Docker

You can create a docker image by calling the Dockerfile using :

```
docker build -t webapp .
```

And then running the image by executing :

```
docker run -p 80:3000 -d webapp  
```

The server will be available at http://\<docker_url\>:80/ 